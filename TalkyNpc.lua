local Methods = {}

function Methods.say(pid, line, name)   
    if name ~= nil then
        mpWidgets.Notification(pid, string.format("#ff9d00[%s]#ffffff: %s", name, line))
    else
        mpWidgets.Notification(pid, line)
    end
end

function Methods:say_by_script(pid, refId, script)  
    local player = Players[pid]
    
    if player.npc_lines == nil then
        Players[pid].npc_lines = {}
        Players[pid].npc_lines[refId] = 0
    end
    
    local num = Players[pid].npc_lines[refId]
    
    if num < #script then
        num = num + 1
        Players[pid].npc_lines[refId] = num
    end
    
    self.say(pid, script[num])
end

function Methods.say_random(pid, script)
    local num = luastd.Random.hw(1, #script)
    mpWidgets.Notification(pid, script[num])
end

function Methods.say_voice(cell, refId, file, line)
    raise.console.run_in_cell(cell, {string.format('"%s" -> say "%s" "%s"', refId, file, line)})
end

function Methods.say_voice_player(pid, file, line)
    tes3mp.PlaySpeech(pid, file)
    mpWidgets.Notification(pid, line)
end

return Methods
